package io.kabir.mock.controller;

import io.kabir.mock.model.Person;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MockController {
    @GetMapping("/")
    public String getRoot() {
        return "Root";
    }

    @GetMapping("/persons/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable("id") String id) {
        Integer personId = Integer.valueOf(id);
        switch (personId) {
            case 5:
                return new ResponseEntity<Person>(new Person(personId, "Shayan Kabir"), HttpStatus.OK);
            case 6:
                return new ResponseEntity<Person>(new Person(personId, "Mohammad Kabir"), HttpStatus.OK);
            case 7:
                return new ResponseEntity<Person>(new Person(personId, "Shah Asad"), HttpStatus.OK);
            case 8:
                return new ResponseEntity<Person>(new Person(personId, "Mohammad Zahangir"), HttpStatus.OK);
            case 9:
                return new ResponseEntity<Person>(new Person(personId, "Iftear Zahid"), HttpStatus.OK);
            case 10:
                return new ResponseEntity<Person>(new Person(personId, "Ahmed Ferdous"), HttpStatus.OK);
            default:
                return new ResponseEntity<>(new Person(0, ""),HttpStatus.NOT_FOUND);
        }
    }
}